<?php

	/* Include theme functions */
	$function_dir_path = get_stylesheet_directory() . '/functions';

	$function_file_path = [
		'/basic/theme-supports.php',
		'/basic/enqueue_styles_scripts.php',
		'/helper.php',
		'/basic/custom-post-types.php',
		'/basic/custom-taxonomy.php',
		'/basic/custom-menus.php',
		'/basic/widgets.php',
		'/advanced/wp-bootstrap-navwalker.php',
		'/advanced/actions-filters.php',
		'/advanced/client-roles.php',
		'/advanced/shortcodes.php',
		'/include/template-override.php',
		'/components/tinymce.php'
	];

	if($function_file_path && is_array($function_file_path)){
		foreach ($function_file_path as $key => $value) {
			if (file_exists($function_dir_path . $value)) {
			    require_once($function_dir_path . $value);
			}
		}
	}

	require_once get_stylesheet_directory() . '/components/main.php';


	/* end */

?>