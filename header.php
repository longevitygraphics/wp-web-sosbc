<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

<header id="masthead" class="sticky-header">
  
  <div class="header-content d-flex flex-column">

    <?php get_template_part("/templates/template-parts/header/site-utility-bar"); ?>

    <div class="bg-secondary order-1 order-md-2 header-main">
        <div class="align-items-center">
          <div> <?php get_template_part('templates/template-parts/nav-main'); ?> </div>
        </div>
    </div>
  </div>

</header>
