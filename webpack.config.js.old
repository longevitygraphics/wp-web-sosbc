const path = require("path");
const autoprefixer = require("autoprefixer");
const MiniCSSExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
//const CopyWebpackPlugin = require('copy-webpack-plugin');
const LiveReloadPlugin = require("webpack-livereload-plugin");

module.exports = (env, argv) => {
    var config = {
        entry: {
            main: "./assets/entry.js",
            editor: "./assets/src/js/editor.js",
        },
        output: {
            path: path.join(__dirname, "assets/dist/js"),
            filename: "[name].js",
        },
        optimization: {
            minimizer: [
                new TerserPlugin({
                    sourceMap: true,
                }),
                new OptimizeCSSAssetsPlugin({
                    cssProcessorOptions: {
                        map: {
                            //inline: false,
                            annotation: true,
                        },
                    },
                }),
            ],
        },
        plugins: [
            new LiveReloadPlugin(),
            new CleanWebpackPlugin(),
            new MiniCSSExtractPlugin({
                filename: "../css/[name].css",
            }),
            /* new CopyWebpackPlugin([
         { from: path.join(__dirname, "assets/src/images"), to: path.join(__dirname, "assets/dist/images") }
       ]),*/
        ],
        devtool:
            argv.mode === "development"
                ? "cheap-module-source-map"
                : "source-map",
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ["@babel/preset-env"],
                        },
                    },
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCSSExtractPlugin.loader,
                        "css-loader",
                        {
                            loader: "postcss-loader",
                            options: {
                                plugins: [autoprefixer()],
                            },
                        },
                        "sass-loader",
                    ],
                },
                {
                    test: /\.(eot|woff|woff2|ttf)$/,
                    use: {
                        loader: "url-loader",
                        options: {
                            limit: 10000,
                            name: "../fonts/[name].[ext]",
                        },
                    },
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: {
                        loader: "url-loader",
                        options: {
                            limit: 10000,
                            name: "../images/[name].[ext]",
                        },
                    },
                },
                {
                    test: /\.(svg)$/,
                    use: {
                        loader: "url-loader",
                        options: {
                            limit: false,
                            name: "../images/[name].[ext]",
                        },
                    },
                },
            ],
        },
    };
    return config;
};
