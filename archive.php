<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				<div class="container body-copy">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<!-- Main Loop -->
							<h1><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h1>
							<hr>
							<p><?php the_excerpt(); ?></p> 
						<?php endwhile; ?>
					<?php endif ?>
				</div>
			</main>
		</div>
	</div>

<?php get_footer(); ?>