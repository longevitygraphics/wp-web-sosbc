<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<div id="primary">
		<main id="content" role="main" class="site-content">

				<?php get_template_part( '/components/acf-flexible-layout/layouts' ); ?>

				<?php get_template_part('/templates/template-parts/page/blog-categories-list') ?>

				<?php get_template_part('/templates/template-parts/page/blog-loop') ?>

		</main>
	</div>

<?php get_footer(); ?>