<div class="list-pipe">
	<ul>
		<li><?php bloginfo('name'); ?></li>
		<li><?php echo do_shortcode('[lg-address1]'); ?>, <?php echo do_shortcode('[lg-city]'); ?>,&nbsp;<?php echo do_shortcode('[lg-province]'); ?>, <?php echo do_shortcode('[lg-country]'); ?> <?php echo do_shortcode('[lg-postcode]'); ?></li>
		<li><a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]'), 'Brackets'); ?></a></li>
		<li><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></li>
		<li>Registered Canadian Charity #: 12993-5011 RR0001</li>
		<li><a href="/privacy-policy">Privacy Policy</a></li>
		<li><span>&copy; <?php echo date("Y") ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span></li>
	</ul>
		<div class="mb-2">Located on the unceded traditional territories of the Salish Peoples, including the q̓ic̓əy̓ (Katzie), q̓ʷɑ:n̓ƛ̓ən̓ (Kwantlen), and Semiahma (Semiahmoo) land-based nations</div>
		<?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?>
</div>