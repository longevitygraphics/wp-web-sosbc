<div class="popup-wrap leave-popup">
	<div class="px-4 py-3 popup">
		<div>
			<h2 class="text-blue">Thank you so much for visiting our website!</h2>
			<p>Before you go, would you join the 1,500 supporters and counting subscribed to the SOS Children's Village BC e-newsletter?</p>
			
			<div id="donorengine-form-popup"></div>

			
			<p>or</p>
			<a href="" class="btn-blue close-popup">Close Window</a>
		</div>
	</div>
</div>