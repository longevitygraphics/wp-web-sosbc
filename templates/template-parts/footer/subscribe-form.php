<div class="py-3 bg-secondary subscribe-form text-white">
	<div class="container">
		<div class="text-center">
			<div class="">
				<span class="h4">SIGN UP FOR OUR NEWSLETTER AND UPDATES</span>
			</div>
			<div class="">
				<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
			</div>
		</div>
	</div>
</div>
