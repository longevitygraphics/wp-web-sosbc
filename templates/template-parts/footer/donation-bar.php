<div class="col col-12 col-sm-4"><h2 class="h5">GIVE WHERE NEEDED MOST</h2></div>
<div class="col col-12 col-sm-8">
	<ul class="list-inline">
		<li class="list-inline-item">
			<a href="https://de.sosbc.org/donations/donate">
				<span value="" class="btn-pink font-weight-bold" id="donate-25">$25</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="https://de.sosbc.org/donations/donate">
				<span value="" class="btn-brown font-weight-bold" id="donate-50">$50</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="https://de.sosbc.org/donations/donate">
				<span value="" class="btn-pink font-weight-bold" id="donate-100">$100</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="https://de.sosbc.org/donations/donate">
				<span value="" class="btn-brown font-weight-bold" id="donate-250">$250</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="https://de.sosbc.org/donations/donate">
				<span value="" class="btn-pink font-weight-bold" id="donate-500" >$500</span>
			</a>
		</li>
		<li class="list-inline-item">
			<a href="https://de.sosbc.org/donations/donate" class="btn-brown font-weight-bold text-uppercase" id="donate-designation">Donate</a>
		</li>
	</ul>
</div>
