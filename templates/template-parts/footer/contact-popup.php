<?php

	$pages = get_field('contact_popup_on_page', 'option');

?>

<?php if($pages && is_array($pages) && in_array($post->ID, $pages)): ?>

<script type="text/javascript">// <![CDATA[
  window.liveSiteAsyncInit = function() {
    LiveSite.init({
      id : 'WI-OLQCHBLBW2P2I5SO96OU'
    });
  };
  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0],
        p = (('https:' == d.location.protocol) ? 'https://' : 'http://'),
        r = Math.floor(new Date().getTime() / 1000000);
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = p + "www.vcita.com/assets/livesite.js?" + r;
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'vcita-jssdk'));
// ]]></script>

<?php endif; ?>