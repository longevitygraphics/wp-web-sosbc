<div class="utility-bar container-fluid text-white bg-dark order-2 order-md-1">
	<div class="d-flex justify-content-between">
		<div>
			<?php echo do_shortcode('[gtranslate]'); ?>
		</div>
		<div class="row">
			<div class="col">
					<?php wp_nav_menu( array(
						'theme_location' => 'utility-nav',
						'depth'          => '1',
						'container'      => 'nav',
						'menu_class'     => 'nav justify-content-center justify-content-md-end'
					) ); ?>
			</div>
		</div>
  </div>
</div>
