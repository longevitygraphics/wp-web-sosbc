<!-- Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-secondary text-white">
        <?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
        <!-- <p class="modal-title ml-5" id="searchModalLabel"><span class="">Search</span></p> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <?php get_search_form(); ?>
      </div>
    </div>
  </div>
</div>