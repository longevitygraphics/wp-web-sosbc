<nav class="navbar navbar-expand-lg navbar-dark">

	<div class="nav-brand px-2">
		<div class="py-1"><?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?></div>
		<div class="donate-btn bg-pink text-white d-none-1024">
			<a href="https://de.sosbc.org/donations/donate">Donate Now</a>
		</div>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-main" aria-controls="nav-main" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	</div>

	<div class="navigation">
		<?php
			wp_nav_menu( array(
				// 'theme_location'    => 'primary',
				'depth'           => 2,
				'theme_location'  => 'top-nav',
				'container'       => 'div',
				'container_class' => 'collapse navbar-collapse',
				'container_id'    => 'nav-main',
				'menu_class'      => 'nav navbar-nav ml-auto',
				'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
				'walker'            => new WP_Bootstrap_Navwalker(),
			) );
		?>
	</div>
	<div class="d-lg-none">
		
	</div>
</nav>
