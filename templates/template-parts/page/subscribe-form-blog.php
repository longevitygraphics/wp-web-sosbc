<div class="py-4 bg-green">
	<div class="container">
		<div class="h3 text-white text-uppercase mb-0">Subscribe To Receive New Blog Posts</div>
		<?php echo do_shortcode('[gravityform id=8 title=false description=false ajax=true tabindex=49]'); ?>
	</div>
</div>