<div class="py-5 blog-loop">
	<div class="container">
		<h2 class="text-orange text-uppercase mb-3">GOOD THINGS ARE MEANT TO BE SHARED</h2>
	</div>

	<?php
		$args = array(
	        'showposts'	=> -1,
	        'post_type'		=> 'post',
	    );

	    $result = new WP_Query( $args );
	    $colour = ['red', 'orange', 'green', 'blue'];

	    // Loop
	    if ( $result->have_posts() ) :
	    	$index = 0
	    	?>
	    	<div class="container">
				<div class="blog-posts ">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$date = get_the_date();
		    	$content = short_string(get_field('content'), 40) . '...';
		    	$author = get_author_name();
		    	$thumbnail = get_the_post_thumbnail_url();
		    ?>
		    	
		        <div class="post">
					<div class="blog-img">
		        		<img src="<?php echo $thumbnail; ?>">
					</div>
		        	<div class="details px-3 py-3 text-white bg-<?php echo $colour[$index % sizeof($colour)]; ?>">
		        		<div class="h4"><?php echo $date; ?> </div>
		        		<div class="h4 mb-0"><?php echo $title; ?></div>
		        		<div class="description py-3">
		        			<?php echo $content; ?>
		        		</div>
		        		<a href="<?php echo $link; ?>" class="h4">Read More --></a>
		        	</div>
		        </div>

				<?php
					$index++;
		        	endwhile;
		        ?>
		        </div>
	        </div>
	    <?php else: ?>
	    <p class="center pt-xs">No blog post available.</p>
	        <?php

	    endif; // End Loop

	    wp_reset_query();
	?>
</div>