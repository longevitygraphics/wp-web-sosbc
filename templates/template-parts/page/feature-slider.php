<?php if( have_rows('sosbc_sliders') ): ?>
   <div class="page-slider">
        <?php while ( have_rows('sosbc_sliders') ) : the_row(); 
            $sosbc_slider = get_sub_field('sosbc_slider');
            $image = $sosbc_slider['image'];
            $link = $sosbc_slider['link'];

            $sosbc_slider_overlay = get_sub_field('sosbc_slider_overlay');
            $content = $sosbc_slider_overlay['content'];
            $button = $sosbc_slider_overlay['button'];
        ?>
            <div>
            	<div class="img-cont">
    	             <a href="<?php if($link){echo $link['url'];} ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
            	</div>
                <div class="slider-content">
                    <div class="slide-content bg-dark py-2 px-3 text-white">
                        <h2><?php echo $content; ?></h2>
                    </div>
                    <a class="btn btn-secondary" href="<?php echo $button['url']; ?>"><?php echo $button['title']; ?></a>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
