<div class="our-partners">
	<div class="container pt-5">
		<h2 class="text-primary text-center mb-4">
			<strong>Our Partners</strong>
		</h2>


		<div style="position: relative;">
			<a class="partner-arrow" href="/who-we-are/our-partners"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/arrows.png' ?>"></a>
		
		<?php
		if( have_rows('partner_logos', 'option') ):
			?>
			<div class="partner-logo">
			<?php
		    while ( have_rows('partner_logos', 'option') ) : the_row();
		        $logo = get_sub_field('logo');
		        $link = get_sub_field('link');
		        ?>
		        	<div>
		        		<a href="<?php echo $link['url']; ?>"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"></a>
		        	</div>
		        <?php
		    endwhile;
		    ?>
		    
		    </div>
		    <?php
		else :
		    // no rows found
		endif;
		?>
	</div>
	</div>
</div>

<?php

	$partner_testimonial = get_field('partner_testimonial', 'option');

?>
	<div class="pt-3 pb-5 container text-center our-testimonials">
		<strong class="h4"><?php echo $partner_testimonial['content']; ?></strong>
		<?php echo $partner_testimonial['author']; ?>
	</div>