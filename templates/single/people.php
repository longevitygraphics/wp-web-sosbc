<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				
				<?php

				$name = get_the_title();
				$thumbnail = get_the_post_thumbnail_url();
				$certs = get_field('certs');
				$title = get_field('title');
				$company = get_field('company');
				$cell = get_field('cell');
				$extension = get_field('extension');
				$email = get_field('email');
				$short_description = get_field('short_description');

				?>

				<div class="py-5 container people text-green">
					<div class="row">
						<div class="col-md-4 col-sm-5">
							<?php if($thumbnail): ?>
								<img src="<?php echo $thumbnail; ?>" class="img-full">
							<?php endif; ?>	
						</div>
						<div class="col-md-8 col-sm-6">
					    	<div class="profile mt-1 mt-md-0">
								<div>
									<?php if($name): ?>
										<?php if($certs): ?>
											<div class="name h3 mb-0"><strong><?php echo $name; ?>, <?php echo $certs; ?></strong></div>
										<?php else: ?>
											<div class="name h3 mb-0"><strong><?php echo $name; ?></strong></div>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($title): ?>
										<div class="title mb-0"><?php echo $title; ?></div>
									<?php endif; ?>
								</div>
								<div class="mt-1">
									<?php if($company): ?>
										<div class="company"><?php echo $company; ?></div>
									<?php endif; ?>
									<?php if($cell): ?>
										<div class="cell"><a href="tel:<?php echo $cell; ?>"><?php echo $cell; ?> <?php if($extension){echo ' ext. '.$extension;} ?></a></div>
									<?php endif; ?>
									<?php if($email): ?>
										<div class="email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
									<?php endif; ?>
								</div>
								<?php if($short_description): ?>
									<div class="short_description pt-2 pb-0" style="color: initial;"><?php echo $short_description; ?></div>

								<?php endif; ?>
					    	</div>
						</div>
					</div>
			    	
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>