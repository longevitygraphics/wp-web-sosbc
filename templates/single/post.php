<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>
test
	<div id="primary">
		<main id="content" role="main" class="site-content container py-4">

			<div class="sidebar-main">
				<div class="sidebar-body">
					
					<?php

					$title = get_the_title();
			    	$link = get_permalink();
			    	$date = get_the_date();
			    	$content = get_field('content');
			    	$author = get_author_name();
			    	$thumbnail = get_the_post_thumbnail_url();

			    	?>


					<div class="single-post">
			        	<img src="<?php echo $thumbnail; ?>">

			        	<div class="details py-3">
			        		<h2 class="h3 mb-0 text-blue"><strong><?php echo $title; ?></strong></h2>
			        		<div><i><?php echo $date; ?> by <?php echo $author; ?></i></div>
			        		<div class="description py-3">
			        			<?php echo $content; ?>
			        		</div>
			        	</div>

			        	<?php if(get_next_post() || get_previous_post()): ?>
				        	<div class="blog-nav">
								<?php if(get_next_post()): ?>
									<a href="<?php echo get_next_post()->guid; ?>" class="btn-blue previous">Previous Post</a>
								<?php endif; ?>
								<?php if(get_previous_post()): ?>
									<a href="<?php echo get_previous_post()->guid; ?>" class="btn-blue next">Next Post</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
			        </div>

				</div>
				
				<?php get_sidebar(); ?>
			</div>
		</main>
	</div>

<?php get_footer(); ?>