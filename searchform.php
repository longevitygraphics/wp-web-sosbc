	<form id="searchform" method="get" action="/index.php">
		<div class="input-group input-group-lg mb-3">
			<input name="s" id="s" type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
			<div class="input-group-append">
				<button type="submit" class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i><span class="sr-only">search</span></button>
			</div>
		</div>
	</form>
	