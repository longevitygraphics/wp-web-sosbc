<?php

	// Register format button
	function my_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


	// Insert items to format button
	function my_mce_before_init_insert_formats( $init_array ) { 

		$colors = ['white', 'blue', 'red', 'green', 'orange', 'pink', 'gold'];
		$title_size = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
		$margin = array(
			(object) array("title"=> "No Padding", "value"=> "0"),
			(object) array("title"=> "Extra Small", "value"=> "1"),
			(object) array("title"=> "Small", "value"=> "2"),
			(object) array("title"=> "Medium", "value"=> "3"),
			(object) array("title"=> "Large", "value"=> "4"),
			(object) array("title"=> "Extra Large", "value"=> "5")
		);

		$color_array = [];
		$button_array = [];
		$title_size_array = [];
		$margin_top = [];
		$margin_bottom = [];

		if($title_size && is_array($title_size)){
			foreach ($title_size as $key => $value) {
				array_push($title_size_array, array(
					'title' => ucwords($value),
		            'selector' => 'span,div,h1,h2,h3,h4,h5,h6,p,a',
		            'classes' => $value
				));
			}
		}

		if($colors && is_array($colors)){
			foreach ($colors as $key => $value) {
				array_push($color_array, array(
					'title' => ucwords($value),
		            'selector' => 'span,div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
		            'classes' => 'text-'.$value
				));
			}
		}

		if($colors && is_array($colors)){
			foreach ($colors as $key => $value) {
				array_push($button_array, array(
					'title' => ucwords($value),
		            'selector' => 'a',
		            'classes' => 'btn-'.$value
				));
			}
		}

		if($margin && is_array($margin)){
			foreach ($margin as $key => $value) {
				array_push($margin_top, array(
					'title' => $value->title,
		            'selector' => 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
		            'classes' => 'mt-'.$value->value
				));
			}
		}

		if($margin && is_array($margin)){
			foreach ($margin as $key => $value) {
				array_push($margin_bottom, array(
					'title' => $value->title,
		            'selector' => 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
		            'classes' => 'mb-'.$value->value
				));
			}
		}


	    $style_formats = array(
	    	array(
	            'title' => 'Title Size',
	            'items' =>  $title_size_array
	        ),
	        array(
	            'title' => 'Colors',
	            'items' =>  $color_array
	        ),
	        array(
	            'title' => 'Buttons',
	            'items' =>  $button_array
	        ),
	        array(
	            'title' => 'Spacing Top',
	            'items' =>  $margin_top
	        ),
	        array(
	            'title' => 'Spacing Bottom',
	            'items' =>  $margin_bottom
	        ),
	        array(
	            'title' => 'Image',
	            'items' =>  array(
	            	array(
						'title' => 'Full Width',
			            'selector' => 'img',
			            'classes' => 'img-full'
					)
	            )
	        ),
	        array(
	            'title' => 'List',
	            'items' =>  array(
	            	array(
						'title' => 'Arrow',
			            'selector' => 'ul, ol',
			            'classes' => 'arrow-list'
					),
					array(
						'title' => 'Check',
			            'selector' => 'ul, ol',
			            'classes' => 'check-list'
					)
	            )
	        )
	    );

	    $init_array['style_formats'] = json_encode( $style_formats );

	    return $init_array; 
	  
	} 

	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

?>