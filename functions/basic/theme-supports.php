<?php

function add_template_support() {

	add_theme_support('post-thumbnails');

	add_theme_support('automatic-feed-links');

	add_theme_support( 'title-tag' );

	add_theme_support( 'custom-logo', array(
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	add_theme_support( 'menus' );

	register_nav_menus( array(
		"top-nav"     => "Top Main",
		"bottom-nav"  => "Bottom Main",
		"utility-nav" => "Top Utility Menu",
	) );



	/*function add_search_form($items, $args) {
	          if( $args->theme_location == 'utility-nav' ){
	          $items .= '<li class="menu-item">'
						. '<a href="#"  data-toggle="modal" data-target="#searchModal">'
						. 'Search'
						. '</a>'
	                  . '</li>';
	          }
	        return $items;
	}
	add_filter('wp_nav_menu_items', 'add_search_form', 10, 2);*/




	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	function add_svg_to_upload_mimes( $upload_mimes ) {
		$upload_mimes['svg'] = 'image/svg+xml';
		$upload_mimes['svgz'] = 'image/svg+xml';
		return $upload_mimes;
	}
	add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );


}

add_action('after_setup_theme','add_template_support', 16);

?>