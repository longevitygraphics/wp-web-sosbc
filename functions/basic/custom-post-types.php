<?php

//Custom Post Types
function create_post_type() {

    // PROGRAMS
    
    register_post_type( 'program',
        array(
          'labels' => array(
            'name' => __( 'Programs' ),
            'singular_name' => __( 'Program' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'sosbc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt', 'revisions' )
        )
    );

    register_post_type( 'what-can-you-do',
        array(
          'labels' => array(
            'name' => __( 'What You Can Do' ),
            'singular_name' => __( 'What You Can Do' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'sosbc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt', 'revisions' )
        )
    );

    register_post_type( 'who-we-are',
        array(
          'labels' => array(
            'name' => __( 'Who We Are' ),
            'singular_name' => __( 'Who We Are' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'sosbc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt', 'revisions' )
        )
    );

    register_post_type( 'the-village',
        array(
          'labels' => array(
            'name' => __( 'The Village' ),
            'singular_name' => __( 'The Village' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'sosbc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt', 'revisions' )
        )
    );

    register_post_type( 'people',
        array(
          'labels' => array(
            'name' => __( 'Peoples' ),
            'singular_name' => __( 'People' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'sosbc',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt', 'revisions' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>