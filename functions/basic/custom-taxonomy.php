<?php

function create_taxonomy(){

	register_taxonomy(
		'people-category',
		'people',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'people-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>