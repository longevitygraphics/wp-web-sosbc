<?php

	function lg_enqueue_styles_scripts() {

		$asset_files = array(
			"style" => include( get_stylesheet_directory() . '/build/frontend.asset.php' ),
			"editorStyle" => include( get_stylesheet_directory() . '/build/editor.asset.php' ),
			"js" => include( get_stylesheet_directory() . '/build/main.asset.php' ),
			"editorJs" => include( get_stylesheet_directory() . '/build/editorjs.asset.php' ),
		);

	    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], wp_get_theme()->get('Version') );
		
		wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i', false );
		wp_enqueue_style( 'vendorCss', get_stylesheet_directory_uri() . '/assets/dist/css/vendor.min.css', false );

		wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), '1.0.1' );
		wp_register_script( 'vendorJS', get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js", array('jquery'), false );
		wp_register_script( 'bootstrapPopper', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js", array('jquery'), false );


		wp_register_style( 'lg-styles-additional', get_stylesheet_directory_uri() . "/build/style-frontend.css", $asset_files['style']['dependencies'] );
		
		wp_enqueue_style('lg-styles-additional');



		wp_enqueue_script( 'bootstrapPopper' );
		wp_enqueue_script( 'lg-script' );
		wp_enqueue_script( 'vendorJS' );
	}
	add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );

	add_editor_style( [
		get_stylesheet_directory_uri() . '/style.css',
		'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i',
		get_stylesheet_directory_uri() . '/assets/dist/css/vendor.min.css'
	] );


	//Dequeue JavaScripts
	function lg_dequeue_scripts() {
		wp_dequeue_script( '_s-navigation' );
		wp_deregister_script( '_s-navigation' );
	}
	//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );

?>