<?php 


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function _s_widgets_init() {

	// Default
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Sidebar', '_s' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);
	

	// Footer Bravo ~ First Item
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Footer Bravo', '_s' ),
			'id'            => 'footer-bravo',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

	// Footer Charlie ~ First Item
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Footer Charlie', '_s' ),
			'id'            => 'footer-charlie',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);


	// Blog Bar
	register_sidebar( 
		array(
			'name'          => esc_html__( 'Blog Green Banner', '_s' ),
			'id'            => 'blog-banner',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>', 
		)		
	);

}
add_action( 'widgets_init', '_s_widgets_init' );

