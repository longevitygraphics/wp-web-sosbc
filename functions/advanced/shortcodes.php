<?php
    
function sos_last_blog_text( $atts, $content = null ) {
    $post = get_posts("post_type=post&numberposts=1");

    ob_start();
    ?>
        <!-- Template code goes here -->

        <?php foreach ($post as $key => $value): 
            $content = short_string(get_field('content', $value->ID), 30) . '...';
            ?>
            <p>
                <?php echo $content; ?>
            </p>
        <?php endforeach; ?>
            
        <!-- end -->
    <?php

    return ob_get_clean(); 
}

add_shortcode('sos-last-blog-text', 'sos_last_blog_text');

?>