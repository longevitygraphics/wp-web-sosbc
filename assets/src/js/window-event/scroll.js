// Window Scroll Handler

(function($) {

    $(window).on('scroll', function(){

    	nav_animation();
    	sticky_header_initialize();
    })

}(jQuery));