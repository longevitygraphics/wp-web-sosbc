// Windows Load Handler

(function ($) {
  $(window).on("load", function () {
    // sticky_header_initialize();
    $("iframe").each(function () {
      if (!$(this).hasClass("embed-responsive-item")) {
        $(this).addClass("embed-responsive-item");
      }
    });

    // $('.blog-posts').masonry({
    //     // options...
    //     itemSelector: '.post',
    //   });
  });
})(jQuery);
