// Windows Ready Handler

(function ($) {
    $(document).ready(function () {
        // Default Page Slider
        $(".page-slider").slick({
            lazyLoad: "ondemand",
            infinite: true,
            autoplay: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 1000,
            fade: true,
            cssEase: "linear",
            arrows: true,
            dots: false,
        });

        // Partner Slider
        $(".partner-logo").slick({
            lazyLoad: "ondemand",
            infinite: true,
            autoplay: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            speed: 1000,
            cssEase: "linear",
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                    },
                },
            ],
        });

        //If Mega Menu
        $(".navbar-toggler").on("click", function () {
            $(".mega-menu-toggle").toggleClass("mega-menu-open");
            $("html, body").toggleClass("locked");
        });

        sticky_header_initialize();
        nav_animation();

        $(".nav-tabs a").on("click", function () {
            $(this)
                .closest("li")
                .addClass("active")
                .siblings()
                .removeClass("active");
        });

        $("html").on("mouseleave", function (e) {
            if (!Cookies.get("leave-popup")) {
                $(".leave-popup").fadeIn();
                $("#donorengine-form").appendTo("#donorengine-form-popup");
                Cookies.set("leave-popup", "1", { expires: 7 });
            }
        });

        $(".popup .close-popup").on("click", function (e) {
            e.preventDefault();
            $(this).closest(".popup-wrap").fadeOut();
            $("#donorengine-form-popup").appendTo(".subscript-form");
        });

        $(".donate-form").submit(function (event) {
            var checked = false;
            $(".radio_button").each(function () {
                if ($(this).attr("checked") == "checked") {
                    checked = true;
                }
            });
            if (
                checked &&
                $(".donate_form_text_input").val() != "" &&
                $(".donate_form_text_input").val()
            ) {
                alert(
                    "ERROR, cannot specify custom amount and default amount at same time"
                );
                return false;
            }
            if (isNaN($(".donate_form_text_input").val())) {
                console.log(isNaN($(".donate_form_text_input")));
                alert("Other $: Must be only numbers");
                return false;
            }
            return true;
        });

        $('.donate-form input[type="radio"][name="P_price1"]').on(
            "change",
            function () {
                if ($(this).attr("checked") == "checked") {
                    $('.donate-form input[type="text"][name="P_price1"]').val(
                        ""
                    );
                }
            }
        );

        $('.donate-form input[type="text"][name="P_price1"]').on(
            "change",
            function () {
                if ($(this).val() != "") {
                    $('.donate-form input[type="radio"][name="P_price1"]').each(
                        function () {
                            console.log($(this));
                            $(this).prop("checked", false);
                        }
                    );
                }
            }
        );
    });
})(jQuery);
