function sticky_header_initialize(){
	var headerHeight = jQuery('#masthead').outerHeight();
	jQuery('#primary').css('paddingTop', headerHeight);

	if(jQuery(window).width() < 768){
		console.log('here');
		jQuery('.max-mega-menu').height('calc( 100vh - ' + jQuery('.nav-brand').outerHeight() + 'px)');
	}else{
		jQuery('.max-mega-menu').height('auto');
	}
}


function nav_animation(){

	var offset_breakpoint = 100;

	if(jQuery(window).scrollTop() > 100 && !jQuery('#masthead').hasClass('scroll-animated')){
		jQuery('#masthead').addClass('scroll-animated');
	}else if(jQuery(window).scrollTop() <= 100 && jQuery('#masthead').hasClass('scroll-animated')){
		jQuery('#masthead').removeClass('scroll-animated');
	}

}