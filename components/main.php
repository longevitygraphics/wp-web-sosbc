<?php

function lg_component_enqueue_styles_scripts() {

    wp_enqueue_style( 'lg-component-style', get_stylesheet_directory_uri() . '/components/assets/css/components.min.css', [], wp_get_theme()->get('Version') );
	
}
add_action( 'wp_enqueue_scripts', 'lg_component_enqueue_styles_scripts' );

?>