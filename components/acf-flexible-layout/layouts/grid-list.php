<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$items = get_sub_field('grid_list');
	$item_per_row = get_sub_field('item_per_row');
	$item_padding = get_sub_field('item_padding');

?>

<?php if($items && is_array($items)): ?>
	<?php foreach ($items as $key => $value): ?>
		<div class="col col-12 col-sm-6 col-md-<?php echo 12 / $item_per_row; ?> d-flex flex-column list-padding <?php echo $value['background_color']; ?>" style="padding: <?php echo $item_padding; ?>"><?php echo $value['content']; ?></div>
	<?php endforeach; ?>
<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>