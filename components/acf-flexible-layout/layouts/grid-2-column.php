<?php 
/**
 * Grid 2 Column Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<?php 
	
		$left_content_type = get_sub_field('left_content_type');
		$right_content_type = get_sub_field('right_content_type');

		$left_content_copy = get_sub_field('left_content_copy');
		$left_content_image = get_sub_field('left_content_image');
		$right_content_copy = get_sub_field('right_content_copy');
		$right_content_image = get_sub_field('right_content_image');

		$left_width = get_sub_field('left_width');
		$right_width = get_sub_field('right_width');

	?> 

	<?php if($left_content_type == 'Text'): ?>
		<div class="grid-2-column order-md-initial col-12 col-md-<?php echo $left_width; ?> order-2"><?php echo $left_content_copy; ?></div>
	<?php else: ?>
		<div class="grid-2-column col-12 col-md-<?php echo $left_width; ?> order-1 order-md-initial">
			<img class="image-full" src="<?php echo $left_content_image['url']; ?>" alt="<?php echo $left_content_image['alt']; ?>">
		</div>
	<?php endif; ?>

	<?php if($right_content_type == 'Text'): ?>
		<div class="grid-2-column order-md-initial col-12 col-md-<?php echo $right_width; ?> order-2"><?php echo $right_content_copy; ?></div>
	<?php else: ?>
		<div class="grid-2-column col-12 col-md-<?php echo $right_width; ?>  order-1 order-md-initial">
			<img class="image-full" src="<?php echo $right_content_image['url']; ?>" alt="<?php echo $left_content_image['alt']; ?>">
		</div>
	<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
