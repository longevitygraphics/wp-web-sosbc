<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

	$tab1_label = get_sub_field('tab1_label');
	$tab1_content = get_sub_field('tab1_content');
	$tab1_background_image = get_sub_field('tab1_background_image');
	$tab2_label = get_sub_field('tab2_label');
	$tab2_content = get_sub_field('tab2_content');
	$tab2_background_image = get_sub_field('tab2_background_image');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="flexible-tab col-12">
		  <!-- Nav tabs -->
		  <div class="nav-tabs-wrap">
			  <div class="container">
			  	<ul class="nav nav-tabs" role="tablist">
				    <li role="presentation" class="active"><a href="#first-tab" aria-controls="first-tab" role="tab" data-toggle="tab" class="btn-orange"><?php echo $tab1_label; ?></a></li>
				    <li role="presentation"><a href="#second-tab" class="btn-orange" aria-controls="second-tab" role="tab" data-toggle="tab"><?php echo $tab2_label; ?></a></li>
				  </ul>
			  </div>
		  </div>

		  <!-- Tab panes -->
		  <div class="tab-content">
		    <div role="tabpanel" class="tab-pane fade show in active" id="first-tab" style="background-image: url('<?php echo $tab1_background_image; ?>	');">
		    	<div>
		    		<?php foreach ($tab1_content as $key => $value): ?>
		    			<div class="tab-item">
		    				<div class="container">
		    					<div class="tab-icon"><img src="<?php echo $value['icon']['url']; ?>"></div>
		    					<div class="tab-content"><?php echo $value['content']; ?></div>
		    				</div>
		    			</div>
		    		<?php endforeach; ?>
		    	</div>
		    </div>
		    <div role="tabpanel" class="tab-pane fade " id="second-tab" style="background-image: url('<?php echo $tab2_background_image; ?>');">
		    	<div>
		    		<?php foreach ($tab2_content as $key => $value): ?>
		    			<div class="tab-item">
			    			<div class="container">
			    				<div class="tab-content"><?php echo $value['content']; ?></div>
			    				<div class="tab-icon"><img src="<?php echo $value['icon']['url']; ?>"></div>
			    			</div>
			    		</div>
		    		<?php endforeach; ?>
		    	</div>
		    </div>
		  </div>
	</div>	

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
