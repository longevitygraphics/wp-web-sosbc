<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

	$button_types = get_sub_field('button_types');
	$text_colour = get_sub_field('text_colour');
	$choices = get_sub_field('choices');
	$price = get_sub_field('price');
	$button_text = get_sub_field('button_text');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="form-donate col-12">
		<form action="https://www.sagepayments.net/processing/order.asp" accept-charset="UNKNOWN" enctype="application/x-www-form-urlencoded" method="post" class="web-form <?php echo $text_colour; ?>">
			<input name="M_id" type="hidden" value="567375912865" /> 
			<input name="M_image" type="hidden" value="/graphics/567375912865.jpg" /> 
			<input name="F_font" size="12" type="hidden" value="Arial Narrow" /> 
			<input name="F_color" type="hidden" value="FFFFFF" /> 
			<input name="M_color" type="hidden" value="009ee0" /> 
			<input name="BF_color" type="hidden" value="000000" /> 
			<input name="B_color" type="hidden" value="FFFFFF" /> 
			<input name="P_count" type="hidden" value="1" />
			<strong>Quantity:</strong>
			<input class="ml-2" name="P_qty1" size="2" type="text" value="1" /><br />&nbsp;<br />
			<strong>Dinner Preference:</strong>
			<?php if($choices && is_array($choices)): ?>
				<select class="ml-2" name="P_desc1">
					<?php foreach ($choices as $key => $value): ?>
						<option value="<?php echo $value['description']; ?>"><?php echo $value['title']; ?></option>
					<?php endforeach; ?>
				</select>
			<?php endif; ?>
			<input name="P_part1" type="hidden" />
			<input name="P_price1" type="hidden" value="<?php echo $price; ?>" /><br />&nbsp;<br />
			<input class="<?php echo $button_types; ?> mt-2" name="B1" type="submit" value="<?php echo $button_text; ?>" />
		</form>
	</div>	

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
