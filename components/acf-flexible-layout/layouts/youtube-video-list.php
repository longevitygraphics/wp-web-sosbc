<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$videos = get_sub_field('videos');
	$item_per_row = get_sub_field('item_per_row');

?>

<?php if($videos && is_array($videos)): ?>
	<?php foreach ($videos as $key => $value): ?>
		<div class="col col-12 col-sm-6 py-1 col-md-<?php echo 12 / $item_per_row; ?> d-flex flex-column list-padding">
			<div class="embed-responsive embed-responsive-<?php echo $value['video_aspect_ratio']; ?>">
				<?php echo $value['video']; ?>
			</div>
			<?php if($value['description']): ?>
				<div class="py-2" style="font-size: 85%;">
					<?php echo $value['description']; ?> 
				</div>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>