<?php 
/**
 * People List Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$category = get_sub_field('category');
	$item_per_row = get_sub_field('item_per_row');
	$active_fields =  get_sub_field('active_fields');
	$text_color = get_sub_field('text_color');
?>

	<?php

		$args = array(
			'showposts'	=> -1,
			'post_type' => 'people',
			'tax_query' => array(
				array(
					'taxonomy' => 'people-category',
					'field'    => 'term_id',
					'terms'    => $category,
				),
			),
		);
		$result = new WP_Query( $args );

	?>

	<?php if ( $result->have_posts() ) :?>
		<?php while( $result->have_posts() ) : $result->the_post(); 
			$name = get_the_title();
			$thumbnail = get_the_post_thumbnail_url();
			$certs = get_field('certs');
			$title = get_field('title');
			$company = get_field('company');
			$cell = get_field('cell');
			$extension = get_field('extension');
			$email = get_field('email');
			$short_description = get_field('short_description');
		?>

		    <div class="col col-12 col-sm-6 col-md-<?php echo 12 / $item_per_row; ?> list-padding people <?php echo $text_color; ?>">
		    	<?php if($thumbnail && (array_search('thumbnail', $active_fields) !== FALSE)): ?>
					<img src="<?php echo $thumbnail; ?>" class="img-full">
				<?php endif; ?>	
		    	<div class="profile py-3">
					<div>
						<?php if($name && (array_search('name', $active_fields) !== FALSE)): ?>
							<?php if($certs && (array_search('certs', $active_fields) !== FALSE)): ?>
								<div class="name h4 mb-0"><strong><?php echo $name; ?>, <?php echo $certs; ?></strong></div>
							<?php else: ?>
								<div class="name h4 mb-0"><strong><?php echo $name; ?></strong></div>
							<?php endif; ?>
						<?php endif; ?>
						<?php if($title && (array_search('title', $active_fields) !== FALSE)): ?>
							<div class="title mb-0"><?php echo $title; ?></div>
						<?php endif; ?>
					</div>
					<div class="mt-1">
						<?php if($company && (array_search('company', $active_fields) !== FALSE)): ?>
							<div class="company"><?php echo $company; ?></div>
						<?php endif; ?>
						<?php if($cell && (array_search('cell', $active_fields) !== FALSE)): ?>
							<div class="cell"><a href="tel:<?php echo $cell; ?>"><?php echo $cell; ?> <?php if($extension){echo ' ext. '.$extension;} ?></a></div>
						<?php endif; ?>
						<?php if($email && (array_search('email', $active_fields) !== FALSE)): ?>
							<div class="email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
						<?php endif; ?>
					</div>
					<?php if((array_search('learn_more', $active_fields) !== FALSE) && $short_description): ?>
						<a class="people-learn-more" href="<?php echo get_permalink(); ?>"><strong>Learn More</strong></a>
					<?php endif; ?>
					<?php if($short_description && (array_search('short_description', $active_fields) !== FALSE)): ?>
						<div class="short_description pt-2 pb-0" style="color: initial;"><?php echo $short_description; ?></div>

					<?php endif; ?>

		    	</div>
			</div>

		<?php
	    endwhile;
	endif; // End Loop

	wp_reset_query();
	?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>