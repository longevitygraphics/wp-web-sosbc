<?php
/**
 * Grid Image with Overlay
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$items = get_sub_field('grid_list');
	$item_per_row = get_sub_field('item_per_row');

?>

	<?php if($items && is_array($items)): ?>
		<?php foreach ($items as $key => $value):
			$overlay_content = $value['overlay_content'];
			$overlay_button = $value['overlay_button'];
			$overlay_button_type = $value['button_types'];
		?>
			<div class="grid-overlay" style="width: calc( 100% / <?php echo $item_per_row; ?> )">
				<img class="image-full" src="<?php echo $value['images']['url']; ?>" alt="<?php echo $value['images']['alt']; ?>">
				<?php if($overlay_content): ?>
					<div class="overlay <?php echo str_replace("btn","bg",$overlay_button_type) . '-light'; ?>">
						<h2 class="h4 my-2"><?php echo $overlay_content; ?></h2>
						<?php if($overlay_button): ?>
							<a class="mb-2 btn-blue" target="<?php echo $overlay_button['target'] ?>" href="<?php echo $overlay_button['url']; ?>"><?php echo $overlay_button['title']; ?></a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
