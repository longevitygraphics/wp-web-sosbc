<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

	$button_types = get_sub_field('button_types');
	$text_colour = get_sub_field('text_colour');
	$message = get_sub_field('message');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="form-donate col-12">
		<form action="https://www.sagepayments.net/processing/order.asp" accept-charset="UNKNOWN" enctype="application/x-www-form-urlencoded" method="post" class="web-form donate-form <?php echo $text_colour; ?>">
			<input name="M_id" type="hidden" value="567375912865" /> <input name="M_image" type="hidden" value="/graphics/567375912865.jpg" /> 
			<input name="F_font" size="11" type="hidden" value="Tahoma" /> 
			<input name="F_color" type="hidden" value="FFFFFF" /> 
			<input name="M_color" type="hidden" value="009ee0" /> 
			<input name="BF_color" type="hidden" value="000000" /> 
			<input name="B_color" type="hidden" value="FFFFFF" /> 
			<input name="P_count" type="hidden" value="1" /> 
			<input name="P_qty1" type="hidden" value="1" /> 
			<input name="P_part1" type="hidden" value="N/A" /> 
			<input name="P_price1" type="radio" value="50" class="radio_button"/><strong class="h3 mb-0">$50</strong> – Can support the most pressing needs of the children in our programs.<br /><br />
			<input name="P_price1" type="radio" value="100" class="radio_button" /><strong class="h3 mb-0">$100</strong> – Can provide a 'welcome home' package for a new arrival to the Village (Pajamas, toys, toiletries, other basics to make them feel welcome, and most importantly, a name plate for their room door).<br /><br />
			<input name="P_price1" type="radio" value="250" class="radio_button" /><strong class="h3 mb-0">$250</strong> – Can fund a session of the Village Learning Club - providing socialization and academic support.<br /><br />
			<input name="P_price1" type="radio" value="500" class="radio_button" /><strong class="h3 mb-0">$500</strong> – Can cover the cost of sports equipment and fees for a year for a child in care. <br /><br />
			<input name="P_price1" type="radio" value="1000" class="radio_button" /><strong class="h3 mb-0">$1,000</strong> – Can provide therapy for a year to help a young person deal with the trauma they have experienced.<br /><br />
			Other: $&nbsp;<input class="ml-2 donate_form_text_input" name="P_price1" size="4" type="text" /><br /><br />
			<?php echo $message; ?>: <br> <br><textarea name="P_desc1" class="" cols="30" rows="10"></textarea> <br>
			<input class="<?php echo $button_types; ?> mt-2" id="donate" name="B1" type="submit" value="Donate Now" />
		</form>
	</div>	

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
