<?php if( have_rows('flexible_content_block') ):

	while ( have_rows('flexible_content_block') ) : the_row();
		
		switch ( get_row_layout()) {

			case 'text_block':
				get_template_part('/components/acf-flexible-layout/layouts/text-block');
			break;

			case 'grid_-_2_column':
				get_template_part('/components/acf-flexible-layout/layouts/grid-2-column');
			break;

			case 'grid_-_list':
				get_template_part('/components/acf-flexible-layout/layouts/grid-list');
			break;

			case 'youtube_video_list':
				get_template_part('/components/acf-flexible-layout/layouts/youtube-video-list');
			break;

			case 'grid_images_with_overlay':
				get_template_part('/components/acf-flexible-layout/layouts/grid-images-with-overlay');
			break;

			case 'full_width_media':
				get_template_part('/components/acf-flexible-layout/layouts/media');
			break;

			case 'people_list':
				get_template_part('/components/acf-flexible-layout/layouts/people-list');
			break;

			case 'form_-_donate':
				get_template_part('/components/acf-flexible-layout/layouts/form-donate');
			break;

			case 'form_-_donate_-_designation':
				get_template_part('/components/acf-flexible-layout/layouts/form-donate-designation');
			break;

			case 'form_-_gala_ticket':
				get_template_part('/components/acf-flexible-layout/layouts/form-gala_ticket');
			break;

			case 'form_-_gala_give':
				get_template_part('/components/acf-flexible-layout/layouts/form-gala_give');
			break;
			case 'tab':
				get_template_part('/components/acf-flexible-layout/layouts/tab');
			break;
		}

	endwhile;

	else : // no layouts found 

endif; ?>