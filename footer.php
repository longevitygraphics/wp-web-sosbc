<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">


		<?php if(!is_front_page()): ?>
			<div class="social-share container pt-4 pb-3">
				<div class="row d-flex align-items-center">
					<div class="col-sm-4"><strong class="text-blue">Share On Social Media</strong></div>
					<div class="col-sm-8"><?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>	</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="subscript-form">
			<div id="donorengine-form"></div>
			<script type="text/javascript">
			// console.log('Will use DE form');
			jQuery(document).ready(function() {
			// console.log('Loading DE form');
				jQuery('#donorengine-form').load('https://de.sosbc.org/forms/embed?form_id=2&ajax=false&embed=true', function(response) {
			// console.log('Loaded Form', response);
				});
			});
		</script>
		</div>

		<div id="site-footer" class="clearfix">

			<div class="container">
				<div class="row align-items-center py-4">
					<div class="col col-12 col-md-6">
						<div class="site-footer-bravo"><?php dynamic_sidebar('footer-bravo'); ?></div>
					</div>
					<div class="col col-12 col-md-6">
						<div class="site-footer-charlie"><?php dynamic_sidebar('footer-charlie'); ?></div>
					</div>
				</div>
			</div>

		</div>


		<div class="donation-bar container-fluid bg-primary text-white">
			<div class="container">
				<div class="row align-items-center">
					<?php get_template_part('/templates/template-parts/footer/donation-bar') ?>
				</div>
			</div>
		</div>

		<div class="container py-4 px-3">
			<?php get_template_part('/templates/template-parts/footer/nav-footer'); ?>
		</div>

		<div id="site-legal" class="container-fluid bg-richblack text-white p-3">
			<div class="container">
				<div class="row">
					<div class="col col-12 text-center"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				</div>
			</div>
		</div>
		
		

	</footer><!-- #colophon -->


	<?php get_template_part('/templates/template-parts/header/search-form'); ?>
	<?php get_template_part('/templates/template-parts/footer/leave-popup'); ?>
	<?php get_template_part('/templates/template-parts/footer/contact-popup'); ?>

<?php wp_footer(); ?>

</body>
</html>
